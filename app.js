const ReactDOM = require('react-dom');
const React = require('react');
const Contact = require('./components/Contact.js');
const data = require('./data.json');


class Component extends React.Component {

  constructor(props) {
    super(props);
      this.state = {
        query: '',
        result: []
      }
  }


  handleQueryChange(event) {
      console.log(event.target.value);
      let result = data.filter(function(item) {
          return (item['firstName'].includes(event.target.value) ||
            item['lastName'].includes(event.target.value) ||
            item['title'].includes(event.target.value) ||
            item['email'].includes(event.target.value) ||
            item['website'].includes(event.target.value));
      });

      this.setState({
          query: event.target.value,
          result: result
      }, function() {
          console.log(this.state.result);
      });
      if (event.target.value.length === 0) {
          this.setState({
              result: []
          });
      }
      // console.log(result);
  }

  render() {
      const contacts = this.state.result.map((contact, i) =>
        <Contact contact={contact} key={i} keyword={this.state.query} />);
      console.log(contacts);
      return (
        <div>
          <div>
            <label>Search input</label>
              <input
                  style={{ height: 30, width: 200 }}
                  type="text"
                  value={this.state.query}
                  onChange={(event) => this.handleQueryChange(event)}
                  placeholder="Search for contact"/>
          </div>
          <div>
              {contacts}
          </div>
        </div>
      );
  }
}

ReactDOM.render(<Component />, document.getElementById('app'))
