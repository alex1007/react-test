const React = require('react');

class Contact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contact: props.contact,
            keyword: props.keyword
        }
    }
    
    render() {
      return (
          <div>
              <h5>Name: {this.state.contact['firstName']} {this.state.contact['lastName']}</h5>
              <p>Title: {this.state.contact['title']}</p>
              <p>Email: {this.state.contact['email']}</p>
              <p>Website: {this.state.contact['website']}</p>
          </div>
      );
    }
}

module.exports = Contact;
